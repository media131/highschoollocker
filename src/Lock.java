import java.util.Scanner;
public class Lock
{
	private static Scanner keyboard = new Scanner(System.in);
	private int num1;
	private int num2;
	private int num3;
	private static int[] dial = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39};	
	
	public Lock(int num1, int num2, int num3)
	{
		this.num1=num1;
		this.num2=num2;
		this.num3=num3;
	}
	public Lock()
	{	this.num1=0;
		this.num2=0;
		this.num3=0;
	}
	
	
	public void resetDial()
	{
		this.num1=0;
		this.num2=0;
		this.num3=0;
	}
	
	public static int turnLeft(int tempNum)
	{
		System.out.println("You are at dial "+tempNum+", enter a number to turn left.");
		int tempNum2=keyboard.nextInt();
		
		/*
		 * BECAUSE YOU CANT GO OUT OF BOUNDS FOR AN ARRAY
		 * I HAVE TO DESIGN A FORMULA TO CYCLE THROUGH THE ARRAY
		 * 
		 * correct dial = {20,35,25}
		 * tempNum = 20 (20 is the correct first dial, you also moved 20 dials from 0)
		 * tempNum2 = 25 (move 25 backwards not the correct dial is 25, the correct second dial is 35)
		 * get from 20 to 35
		 * move back 25 or move forward 15
		 * 15 is from number of dials - input
		 * so dial 1 + (40 - input)
		 */
		
		if (tempNum2>tempNum)
		{
			tempNum =dial[tempNum+(40-tempNum2)];
		}
		else
		{
			tempNum=dial[tempNum-tempNum2];
		}
		return tempNum;
	}
	public static int turnRight()
	{
		System.out.println("You are at the dial 0, enter a number to turn right.");
		int tempNum=keyboard.nextInt();
		tempNum=dial[0+tempNum];
		
		return tempNum;
	}
	public static int turnRight2(int tempNum)
	{
		/*
		 * BECAUSE YOU CANT GO OUT OF BOUNDS FOR AN ARRAY
		 * I HAVE TO DESIGN A FORMULA TO CYCLE THROUGH THE ARRAY
		 * 
		 * correct dial = {20,35,25}
		 * tempNum = 35 (35 is the correct second dial)
		 * tempNum2 = 30 (move 30 forwards not the correct dial is 30, the correct second dial is 25)
		 * get from 35 to 25
		 * move back 10 or move forward 30
		 * 20 is from number of dials - input
		 * so dial 2 - (40 - input)
		 */
		System.out.println("You are at dial "+tempNum+", enter a number to turn right.");
		int tempNum2=keyboard.nextInt();
		if (tempNum2>(40-tempNum))
		{
			tempNum=dial[tempNum-(40-tempNum2)];
		}
		else
		{
			tempNum=dial[tempNum+tempNum2];
		}
		return tempNum;
	}
	
	public boolean openLock(Lock temp)
	{				
		return 	this.num1==temp.num1
				&& this.num2==temp.num2
				&& this.num3==temp.num3;
	}
	
	public String toString()
	{
		return 	"Combo Lock Numers: ["+num1+", "+num2+", "+num3+"]";
	}
	
	
	
	
	
	
	
	
	public int getNum1()
	{	return num1;	}
	public void setNum1(int num1)
	{	this.num1=num1;	}
	public int getNum2()
	{	return num2;	}
	public void setNum2(int num2)
	{	this.num2=num2;	}
	public int getNum3()
	{	return num3;	}
	public void setNum3(int num3)
	{	this.num3=num3;	}
	
}








