import java.util.Scanner;
public class LockerDemo
{	
	private static Scanner keyboard = new Scanner(System.in);
	private static Locker mickeyLocker;
	private static Locker donaldLocker;
	public static void main(String[] args) 
	{	mickey();	
		donald();
		System.out.println(mickeyLocker);
		System.out.println(donaldLocker);
	}

	private static void mickey() 
	{
		boolean mickeyStatus = false;
		
		Lock mickeyLock = new Lock(28,17,39);		
		mickeyLocker = new Locker("Mickey Mouse", 100, 3, mickeyLock);
		System.out.println(mickeyLocker);
		System.out.println("Need to dial 28 to right, then 11 to left, then 22 to right");
		Lock openMickeyLock = new Lock();
		openMickeyLock.resetDial();
		
		System.out.println("");
		int try1 = Lock.turnRight();
		int try2 = Lock.turnLeft(try1);
		int try3 = Lock.turnRight2(try2);		
		openMickeyLock = new Lock(try1,try2,try3);
		
		System.out.println("You have entered "+openMickeyLock);
		if (openMickeyLock.openLock(mickeyLock))
		{	System.out.println("The combo numbers matched! Lock opened.");
			mickeyStatus = true;
		}
		else
		{	System.out.println("The combo numbers did not match. Lock remained closed.");			
			mickeyStatus = false;
		}
		
		if (mickeyStatus==true)
		{	System.out.println("Mickey has "+mickeyLocker.getBooks()+" books in his locker now.");
			System.out.println("Enter numebr of books to deposit.");		
			int depositBook = keyboard.nextInt();
			mickeyLocker.add(depositBook);
			System.out.println("Mickey has "+mickeyLocker.getBooks()+" books in his locker now.");		
		}
		else
		{	System.out.println("Mickey's locker has not been opened yet.");
			openMickeyLock.resetDial();
			System.out.println("Dial has been reset. Press enter to try again.");
			keyboard.nextLine();
			mickey();
		}
	}
	
	private static void donald() 
	{
		boolean donaldStatus = false;
		Lock donaldLock = new Lock(35,16,27);		//need to dial 35 to right, then 19 to left, then 11 to right
		
		donaldLocker = new Locker("Donald Duck", 275, 0, donaldLock);
		System.out.println(donaldLocker);
		System.out.println("Need to dial 35 to right, then 19 to left, then 11 to right");
		
		
		
		Lock openDonaldLock = new Lock();
		openDonaldLock.resetDial();
		int tryDonald1 = Lock.turnRight();
		int tryDonald2 = Lock.turnLeft(tryDonald1);
		int tryDonald3 = Lock.turnRight2(tryDonald2);
		openDonaldLock = new Lock(tryDonald1,tryDonald2,tryDonald3);
		
		System.out.println("You have entered "+openDonaldLock);
		if (openDonaldLock.openLock(donaldLock))
		{	System.out.println("The combo numbers matched! Lock opened.");
			donaldStatus = true;
		}
		else
		{	System.out.println("The combo numbers did not match. Lock remained closed.");			
			donaldStatus = false;
		}
		
		if (donaldStatus==true)
		{	System.out.println("Donald has "+donaldLocker.getBooks()+" books in his locker now.");
			System.out.println("Enter numebr of books to remove.");		
			int subtractBook = keyboard.nextInt();
			donaldLocker.subtract(subtractBook);
			System.out.println("Donald has "+donaldLocker.getBooks()+" books in his locker now.");		
		}
		else
		{	System.out.println("Donald's locker has not been opened yet.");
			openDonaldLock.resetDial();
			System.out.println("Dial has been reset. Press enter to try again.");
			keyboard.nextLine();
			donald();
		}
	}
}
