/*
	
	public class Book{
	
	private String title;
	private String author;
	private int numberOfPages;
	
	public Book(String title, String author, int pageNumber)
	{	
		this.title = title;
		this.author = author;
		this.numberOfPages = pageNumber;
	}	
	
	public String toString()
	{	
		return	"\n"+"Name: "+name+"\n"+
				"Locker Number: "+lockerNumber+"\n"+
				"Books: "+Arrays.toString(books);
	}	
}
	
 */
public class Locker 
{
	private String name;
	private int lockerNumber;
	private int books;
	private Lock newLock = new Lock();
	
	public Locker(String name, int lockerNumber, int books, Lock newLock)
	{	
		this.name = name;
		this.lockerNumber = lockerNumber;
		this.books = books;
		this.newLock=newLock;
	}
	
	public void add(int depositBook)
	{
		this.books=this.books+depositBook;	
	}
	public void subtract(int subtractBook)
	{
		if (this.books<subtractBook)
		{
			System.out.println("There aren't enough books to take "+subtractBook+" books away.");
		}
		else
		{
			this.books=this.books-subtractBook;	
		}
		
	}
	
	public String toString()
	{	
		return	"\n"+"Name: "+name+"\n"+
				"Locker Number: "+lockerNumber+"\n"+
				"Books: "+books+"\n"+
				"\n"+newLock;
	}
	
	
	
	public String getName() 
	{	return name;	}
	public void setName(String name)
	{	this.name = name;	}
	public int getLockerNumber() 
	{	return lockerNumber;	}
	public void setLockerNumber(int lockerNumber)
	{	this.lockerNumber = lockerNumber;	}
	public int getBooks() 
	{	return books;	}
	public void setBooks(int books) 
	{	this.books = books;	}
	public Lock getNewLock() 
	{	return newLock;	}
	public void setNewLock(Lock newLock) 
	{	this.newLock = newLock;	}
	
	
	
	
}






























